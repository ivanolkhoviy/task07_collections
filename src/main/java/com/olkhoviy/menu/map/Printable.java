package com.olkhoviy.menu.map;

public interface Printable {
    void print();
}
