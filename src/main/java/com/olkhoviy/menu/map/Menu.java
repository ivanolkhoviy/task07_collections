package com.olkhoviy.menu.map;

import java.util.*;

public class Menu {

    Scanner scanner = new Scanner(System.in);
    private Map<String, Printable> methodsMenu;
    private Map<String, String> menu;

    public static void main(String[] args) {
        new Menu().start();
    }

    public Menu() {
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 | action 1");
        menu.put("2", " 2 | action 2");
        menu.put("3", " 3 | action 3");
        menu.put("4", " 4 | action 4");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::action1);
        methodsMenu.put("2", this::action2);
        methodsMenu.put("3", this::action3);
        methodsMenu.put("4", this::action4);
    }

    private void action1() {
        System.out.println("You selected option FIRST");
    }

    private void action2() {
        System.out.println("You selected option SECOND");
    }

    private void action3() {
        System.out.println("You selected option THIRD");
    }

    private void action4() {
        System.out.println("You selected option FOURTH");

    }

    private void showMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            showMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu option: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }
}